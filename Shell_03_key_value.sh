#!/bin/bash

declare -A myArray
myArray=([name]="Noppadol" [age]=26 [city]="Nonthaburi")

# all key-value arrray all element 
echo "${myArray[*]}"

echo "------------------------------------------------"

# get value with key
echo "Name = ${myArray[name]}"
echo "Age = ${myArray[age]}"
echo "City =${myArray[city]}"