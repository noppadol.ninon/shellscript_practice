#!/bin/bash

salary=2900
name="Noppadol"
age=28

echo "My name is $name , Age is $age , Salary is $salary"

name="Robert"
echo "My name is $name"

#var to store the output of a command
HOSTNAME=$(hostname)
echo "Name of this is machine is $HOSTNAME"

#Read only
readonly myjob="Programmer"
echo "My name $name , My job is $myjob"