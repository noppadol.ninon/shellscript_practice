#!/bin/bash

str00="My name is Noppadol , Surname is Nin-On , Age is 36 , Job is Programmer"

#Lenght of String
echo "Lenght of str00 is ${#str00}"

echo "----------------------------------------------------------"

#Upper case
echo "Upper case -> ${str00^^}"

echo "----------------------------------------------------------"

#Lower case
echo "Lower case -> ${str00,,}"

echo "----------------------------------------------------------"

#To Replace a String
str01=${str00/Noppadol/Noppadol-00}
str01=${str00/Nin-On/Nin-On-00}

echo "${str01}"

echo "----------------------------------------------------------"

#To Slice String