#!/bin/bash

#Array
myArray=(1 20 30.5 Hello "Hey Shell Programming")

echo "Element of array 0 is ${myArray[0]}"
echo "Element of array 1 is ${myArray[1]}"
echo "Element of array 2 is ${myArray[2]}"
echo "Element of array 3 is ${myArray[3]}"
echo "Element of arrat 4 is ${myArray[4]}"

echo "-----------------------------------------------------------"

#Display all element of array
echo "all Element of array => ${myArray[*]} "

echo "-----------------------------------------------------------"

#Display lenght of array
echo "lenght of array is ${#myArray[*]}"

echo "-----------------------------------------------------------"

#Display Array from index_start to index_end
echo "Element of array index 2 to 3 index => ${myArray[*]:2:2}"

#Insert element into array
myArray+=(New 30 40)
echo "All value of array after insert => ${myArray[*]}"